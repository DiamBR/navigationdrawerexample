package com.example.gts.navigationdrawerexample

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragments:Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
      return inflater?.inflate(R.layout.fragment_home,null);
    }


     override fun onCreateView(view: View,savedInstanceState: Bundle?){

        super.onViewCreated(view, savedInstanceState)
        button.setOnClickListener {
            Toast.makeText(activity,"You Click",Toast.LENGTH_LONG).show()
        }
    }
}